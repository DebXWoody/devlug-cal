= Unser Stammtisch Programm
Das Programm soll verwendet werden um die Termine der #devlug zu
ermitteln und in verschiedenen Formaten bereitstellen.

* ICS - https://www.devlug.de/devlug-stammtisch-2018.ics
* WEB - https://www.devlug.de/devlug/stammtisch.html#termine
* IRC - Das Programm soll jeden Tag vom Bot aufgerufen werden

== ToDo
=== WEB
Die #devlug Termine durchnummerieren. Wir nennen die devlug dann in
der Regel #devlug #14 / #devlug #15 / ....
Man könnte die Termine auch sortieren. Erst die noch kommenden und
dann die vergangenen. Mit einem Link: https://www.devlug.de/blog/2018-07-24-stammtisch.html



