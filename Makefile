NAME=stammtisch
CXX := g++
CXXFLAGS := -c -g -Wall -static
#LDFLAGS := -g -static -lboost_date_time `pkg-config --libs libical`
LDFLAGS := -g -static /usr/lib/x86_64-linux-gnu/libboost_date_time.a /usr/lib/x86_64-linux-gnu/libical.a 

SRCDIR := src
OBJDIR := obj
BINDIR := bin

cppfiles := $(wildcard $(SRCDIR)/*.cpp)
objects := $(subst $(SRCDIR)/, $(OBJDIR)/, $(cppfiles:.cpp=.o))
deps := $(objetcts:.o=.d)

.PHONEY: all clean

all: $(BINDIR)/$(NAME)
-include $(deps)

$(BINDIR)/$(NAME): $(objects)
	@mkdir -p $(@D)
	$(CXX) $(LDFLAGS) -o $@ $^

$(OBJDIR)/%.d: $(SRCDIR)/%.cpp
	@mkdir -p $(@D)
	$(CXX) -MM -MT "$@ $(patsubst $.d,%.o,$@)" -MF $@ $<

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $< -o $@

clean:
	rm -f $(objects) $(deps)

format:
	clang-format -i $(SRCDIR)/*
