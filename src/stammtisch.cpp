#include "stammtisch_cal.hpp"
#include <stdlib.h>

void printhelp()
{
    std::cout << "Help" << std::endl;
    std::cout << "./stammtisch --ics || --web || --iso || --irc " << std::endl;
}

using namespace devlug::stammtisch;

int main(int argc, char **argv)
{
    if (argc == 2) {
        StammtischCal stammtischCal;
        std::string opt{argv[1]};
        if (!opt.compare("--ics")) {
            OutputICal *output = new OutputICal(&stammtischCal);
            output->print();
            return EXIT_SUCCESS;
        } else if (!opt.compare("--iso")) {
            OutputConsole *output = new OutputConsole(&stammtischCal);
            output->print();
            return EXIT_SUCCESS;
        } else if (!opt.compare("--irc")) {
            OutputIrc *output = new OutputIrc(&stammtischCal);
            output->print();
            return EXIT_SUCCESS;
        } else if (!opt.compare("--web")) {
            return EXIT_SUCCESS;
        } else {
            printhelp();
            return 1;
        }
    } else {
        printhelp();
    }
    return EXIT_SUCCESS;
}
